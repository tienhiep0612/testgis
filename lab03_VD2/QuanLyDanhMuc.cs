﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab03_VD2
{
    public partial class QuanLyDanhMuc : Form
    {
        public QuanLyDanhMuc()
        {
            InitializeComponent();
        }

        string connectionString = "Data Source=localhost;Initial Catalog=QLtest;User ID=sa;Password=Password789";
        int id_selection;
        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void QuanLyDanhMuc_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLtestDataSet1.directory' table. You can move, or remove it, as needed.
            this.directoryTableAdapter.Fill(this.qLtestDataSet1.directory);

        }

        private void add_button_Click(object sender, EventArgs e)
        {
            SqlCommand command;
            SqlCommand command_count;
            string sql;
            string sql_count = "SELECT MAX(iddir) FROM directory";
            int count;
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                // Lấy giá trị lớn nhất của trường iddir từ bảng directory
                command_count = new SqlCommand(sql_count, connection);
                object result = command_count.ExecuteScalar();
                if (result == null || result == DBNull.Value)
                {
                    count = 1;
                }
                else
                {
                    count = Convert.ToInt32(result) + 1;
                }
                // Sửa câu lệnh SQL để chèn dữ liệu vào bảng directory
                sql = "INSERT INTO directory(iddir, namedir) VALUES (@id, @namedir)";

                // Thực thi truy vấn chèn dữ liệu
                using (command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@id", count);
                    command.Parameters.AddWithValue("@namedir", name_text.Text);
                    command.ExecuteNonQuery();
                }

                connection.Close();
                MessageBox.Show("Dữ liệu đã được thêm vào bảng directory.");
                // Tải lại DataGridView
                this.directoryTableAdapter.Fill(this.qLtestDataSet1.directory);
                name_text.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

    }
}
