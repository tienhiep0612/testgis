﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace lab03_VD2
{
    public partial class QLTaiKhoan : Form
    {
        public QLTaiKhoan()
        {
            InitializeComponent();
        }

        string connectionString = "Data Source=localhost;Initial Catalog=QLtest;User ID=sa;Password=Password789";
        int id_selection;
        private void QLTaiKhoan_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLtestDataSet.acc' table. You can move, or remove it, as needed.
            this.accTableAdapter.Fill(this.qLtestDataSet.acc);
        }

        private void add_button_click(object sender, EventArgs e)
        {
            if (AreTextBoxesFilled(username_text, password_text))
            {
                string sql;
                string sql_count = "select max(id) from acc";
                int count;
                SqlConnection connection = new SqlConnection(connectionString);
                try
                {
                    connection.Open();
                    // Lấy giá trị lớn nhất của trường id từ bảng acc
                    SqlCommand command_count = new SqlCommand(sql_count, connection);
                    object result = command_count.ExecuteScalar();
                    if (result == null || result == DBNull.Value)
                    {
                        count = 1;
                    }
                    else
                    {   // Tăng giá trị lên 1 để có giá trị mới cho trường id
                        count = Convert.ToInt32(result) + 1;
                    }
                    // sql injection
                    sql = "insert into acc(id,nameacc,pass) values (@id,@nameacc,@pass)";

                    // Thực thi truy vấn chèn dữ liệu
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", count);
                        command.Parameters.AddWithValue("@nameacc", username_text.Text);
                        command.Parameters.AddWithValue("@pass", password_text.Text);
                        command.ExecuteNonQuery();
                    }

                    connection.Close();
                    MessageBox.Show("Dữ liệu đã được thêm vào bảng acc.");
                    //reload_datagrid_view
                    this.accTableAdapter.Fill(this.qLtestDataSet.acc);
                    username_text.Clear();
                    password_text.Clear();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try { 
                if (e.RowIndex >= 0) // Đảm bảo chỉ lấy dữ liệu khi hàng được chọn, không phải tiêu đề cột
                {
                    DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                    username_text.Text = row.Cells[1].Value.ToString();
                    password_text.Text = row.Cells[2].Value.ToString();
                    id_selection = Convert.ToInt32(row.Cells[0].Value.ToString());
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Không có dữ liệu");
            }
        }
        
        private void search_button_click(object sender, EventArgs e)
        {
            if (AreTextBoxesFilled(username_text, password_text))
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = connection.CreateCommand();

                    // Câu lệnh SQL tìm kiếm
                    string sql = "SELECT * FROM acc WHERE nameacc LIKE @useracc";

                    try
                    {
                        // Mở kết nối
                        connection.Open();

                        // Gán giá trị cho tham số @useracc
                        command.Parameters.AddWithValue("@useracc", "%" + username_text.Text + "%");

                        // Thi hành truy vấn tìm kiếm và nhận kết quả vào một DataTable
                        command.CommandText = sql;

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        dataGridView1.DataSource = dt;
                        connection.Close();
                        username_text.Focus();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
            }
        }

        private void edit_button_click(object sender, EventArgs e)
        {
            if (AreTextBoxesFilled(username_text, password_text))
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = connection.CreateCommand();

                    // Câu lệnh SQL xóa
                    string sql = "UPDATE acc SET pass = @passnew WHERE id = @id";

                    try
                    {
                        if (id_selection > 0)
                        {
                            // Mở kết nối
                            connection.Open();

                            // Lấy giá trị id để sửa từ id_selection (giả sử id_selection là một control nào đó chứa giá trị id cần xóa)
                            int id_edit = id_selection; // Chuyển đổi giá trị sang kiểu số nguyên

                            // Gán giá trị cho tham số @...
                            command.Parameters.AddWithValue("@passnew", password_text.Text);
                            command.Parameters.AddWithValue("@id", id_edit);

                            // Thi hành truy vấn update
                            command.CommandText = sql;
                            command.ExecuteNonQuery();

                            // Đóng kết nối
                            connection.Close();

                            // Thông báo xóa thành công
                            MessageBox.Show("Dữ liệu đã được sửa trong bảng acc.");

                            // Tải lại DataGridView
                            this.accTableAdapter.Fill(this.qLtestDataSet.acc);
                            username_text.Clear();
                            password_text.Clear();
                            username_text.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Không tìm thấy username này vui lòng chọn lại");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
            }
        }
       
        private void delete_button_click(object sender, EventArgs e)
        {
            if (AreTextBoxesFilled(username_text, password_text))
            {
                try
                {
                    // Khởi tạo kết nối và đối tượng lệnh
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        SqlCommand command = connection.CreateCommand();

                        // Câu lệnh SQL xóa
                        string sql = "DELETE FROM acc WHERE id = @id";

                        try
                        {
                            // Mở kết nối
                            connection.Open();
                            if(id_selection > 0) 
                            {
                                // Lấy giá trị id để xóa từ id_selection (giả sử id_selection là một control nào đó chứa giá trị id cần xóa)
                                int id_del = id_selection; // Chuyển đổi giá trị sang kiểu số nguyên

                                // Gán giá trị cho tham số @id
                                command.Parameters.AddWithValue("@id", id_del);

                                // Thi hành truy vấn xóa
                                command.CommandText = sql;
                                command.ExecuteNonQuery();

                                // Đóng kết nối
                                connection.Close();

                                // Thông báo xóa thành công
                                MessageBox.Show("Dữ liệu đã được xóa khỏi bảng acc.");

                                // Tải lại DataGridView
                                this.accTableAdapter.Fill(this.qLtestDataSet.acc);
                                username_text.Clear();
                                password_text.Clear();
                                username_text.Focus();
                            }
                            else
                            {
                                MessageBox.Show("Không tìm thấy username này vui lòng chọn lại");
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Lỗi xảy ra: {ex.Message}");
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
            }
        }

        private void exit_button_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Xác nhận thoát", "Thông báo", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                this.Close();// Đóng form hiện tại (ví dụ: Form chính của ứng dụng)
                // Hoặc có thể sử dụng Application.Exit() để thoát toàn bộ ứng dụng
            }
        }
        
        private bool AreTextBoxesFilled(params System.Windows.Forms.TextBox[] textBoxes)
        {
            foreach (System.Windows.Forms.TextBox textBox in textBoxes)
            {
                if (string.IsNullOrEmpty(textBox.Text))
                {
                    return false; // Trả về false nếu một trong các TextBox không có dữ liệu nhập
                }
            }
            return true; // Trả về true nếu tất cả các TextBox đều có dữ liệu nhập
        }

    }
}
