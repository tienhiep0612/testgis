﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab03_VD2
{
    public partial class QuanLyKhachHang : Form
    {
        public QuanLyKhachHang()
        {
            InitializeComponent();
        }
        private int nextCustomerID = 1; // Biến đếm khách hàng, bắt đầu từ 1

        private void add_Click(object sender, EventArgs e)
        {
            if (check_text()) { 
            ListViewItem listItem = new ListViewItem();

            // Gán mã khách hàng và tăng biến đếm
            listItem.Text = "" + nextCustomerID.ToString();
            nextCustomerID++;

            listItem.SubItems.Add(HoTen.Text);

            RadioButton gtcheck = null;
            foreach (RadioButton gt in GioiTinh.Controls)
            {
                if (gt.Checked)
                {
                    gtcheck = gt;
                    break;
                }
            }

            listItem.SubItems.Add(gtcheck.Text);
            listItem.SubItems.Add(NgaySinh.Text);
            listItem.SubItems.Add(DiaChi.Text);
            listItem.SubItems.Add(SDT.Text);
            listItem.SubItems.Add(Email.Text);
            //add listView
            listView.Items.Add(listItem);

                HoTen.Clear();
                nam.Checked = false;
                nu.Checked = false;
                NgaySinh.Text = "";
                DiaChi.Clear();
                SDT.Clear();
                Email.Clear();
            }
        }
        private Boolean check_text()
        {
            if (string.IsNullOrEmpty(HoTen.Text))
            {
                MessageBox.Show("Chưa nhập họ tên", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (nam.Checked == false && nu.Checked == false)
            {
                MessageBox.Show("Chưa nhập giới tính", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (string.IsNullOrEmpty(DiaChi.Text))
            {
                MessageBox.Show("Chưa nhập địa chỉ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (string.IsNullOrEmpty(SDT.Text))
            {
                MessageBox.Show("Chưa nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (string.IsNullOrEmpty(Email.Text))
            {
                MessageBox.Show("Chưa nhập địa chỉ email", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;

            }
            else
            {
                return true;
            }
        }

        private void reset_Click(object sender, EventArgs e)
        {
            HoTen.Clear();
            nam.Checked = false;
            nu.Checked = false;
            NgaySinh.Text = "";
            DiaChi.Clear();
            SDT.Clear();
            Email.Clear();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Xác nhận thoát", "Thông báo",MessageBoxButtons.OKCancel);
            if(result == DialogResult.OK){
                this.Close();// Đóng form hiện tại (ví dụ: Form chính của ứng dụng)
                // Hoặc có thể sử dụng Application.Exit() để thoát toàn bộ ứng dụng
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
