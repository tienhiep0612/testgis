﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab03_VD2
{
    public partial class QLDanhMuc : Form
    {
        public QLDanhMuc()
        {
            InitializeComponent();
        }

        string connectionString = "Data Source=localhost;Initial Catalog=QLtest;User ID=sa;Password=Password789";
        int id_selection;


        private void QuanLyDanhMuc_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLtestDataSet1.directory' table. You can move, or remove it, as needed.
            this.directoryTableAdapter.Fill(this.qLtestDataSet1.directory);

        }

        private void add_button_Click(object sender, EventArgs e)
        {
            if (AreTextBoxesFilled(name_text))
            {
                string sql;
                string sql_count = "SELECT MAX(iddir) FROM directory";
                int count;
                SqlConnection connection = new SqlConnection(connectionString);
                try
                {
                    connection.Open();
                    // Lấy giá trị lớn nhất của trường iddir từ bảng directory
                    SqlCommand command_count = new SqlCommand(sql_count, connection);
                    object result = command_count.ExecuteScalar();
                    if (result == null || result == DBNull.Value)
                    {
                        count = 1;
                    }
                    else
                    {
                        count = Convert.ToInt32(result) + 1;
                    }
                    // Sửa câu lệnh SQL để chèn dữ liệu vào bảng directory
                    sql = "INSERT INTO directory(iddir, namedir) VALUES (@id, @namedir)";

                    // Thực thi truy vấn chèn dữ liệu
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", count);
                        command.Parameters.AddWithValue("@namedir", name_text.Text);
                        command.ExecuteNonQuery();
                    }

                    connection.Close();
                    MessageBox.Show("Dữ liệu đã được thêm vào bảng directory.");
                    // Tải lại DataGridView
                    this.directoryTableAdapter.Fill(this.qLtestDataSet1.directory);
                    name_text.Clear();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
            }

        }
        private bool AreTextBoxesFilled(params System.Windows.Forms.TextBox[] textBoxes)
        {
            //Duyệt từng textbox trong mảng được thêm
            foreach (System.Windows.Forms.TextBox textBox in textBoxes)
            {
                if (string.IsNullOrEmpty(textBox.Text))
                {
                    return false; // Trả về false nếu một trong các TextBox không có dữ liệu nhập
                }
            }
            return true; // Trả về true nếu tất cả các TextBox đều có dữ liệu nhập
        }

        private void delete_button_Click(object sender, EventArgs e)
        {
            if (AreTextBoxesFilled(name_text))
            {
                try
                {
                    // Khởi tạo kết nối và đối tượng lệnh
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        SqlCommand command = connection.CreateCommand();

                        // Câu lệnh SQL xóa
                        string sql = "DELETE FROM directory WHERE iddir = @id";

                        try
                        {
                            // Mở kết nối
                            connection.Open();
                            if (id_selection > 0)
                            {
                                // Lấy giá trị id để xóa từ id_selection (giả sử id_selection là một control nào đó chứa giá trị id cần xóa)
                                int id_del = id_selection; // Chuyển đổi giá trị sang kiểu số nguyên

                                // Gán giá trị cho tham số @id
                                command.Parameters.AddWithValue("@id", id_del);

                                // Thi hành truy vấn xóa
                                command.CommandText = sql;
                                command.ExecuteNonQuery();

                                // Đóng kết nối
                                connection.Close();

                                // Thông báo xóa thành công
                                MessageBox.Show("Dữ liệu đã được xóa khỏi bảng directory.");

                                // Tải lại DataGridView
                                this.directoryTableAdapter.Fill(this.qLtestDataSet1.directory);
                                name_text.Clear();name_text.Focus();
                            }
                            else
                            {
                                MessageBox.Show("Không tìm thấy namedir này vui lòng chọn lại");
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Lỗi xảy ra: {ex.Message}");
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0) // Đảm bảo chỉ lấy dữ liệu khi hàng được chọn, không phải tiêu đề cột
                {
                    DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                    name_text.Text = row.Cells[1].Value.ToString();
                    id_selection = Convert.ToInt32(row.Cells[0].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không có dữ liệu");
            }
        }
    }
}