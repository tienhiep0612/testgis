﻿namespace lab03_VD2
{
    partial class QLDanhMuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.directoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qLtestDataSet1 = new lab03_VD2.QLtestDataSet1();
            this.search_button = new System.Windows.Forms.Button();
            this.name_text = new System.Windows.Forms.TextBox();
            this.reset_button = new System.Windows.Forms.Button();
            this.add_button = new System.Windows.Forms.Button();
            this.delete_button = new System.Windows.Forms.Button();
            this.exit_button = new System.Windows.Forms.Button();
            this.directoryTableAdapter = new lab03_VD2.QLtestDataSet1TableAdapters.directoryTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.iddirDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namedirDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.directoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLtestDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // directoryBindingSource
            // 
            this.directoryBindingSource.DataMember = "directory";
            this.directoryBindingSource.DataSource = this.qLtestDataSet1;
            // 
            // qLtestDataSet1
            // 
            this.qLtestDataSet1.DataSetName = "QLtestDataSet1";
            this.qLtestDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // search_button
            // 
            this.search_button.Location = new System.Drawing.Point(27, 79);
            this.search_button.Name = "search_button";
            this.search_button.Size = new System.Drawing.Size(110, 45);
            this.search_button.TabIndex = 36;
            this.search_button.Text = "Tìm kiếm";
            this.search_button.UseVisualStyleBackColor = true;
            // 
            // name_text
            // 
            this.name_text.Location = new System.Drawing.Point(27, 42);
            this.name_text.Name = "name_text";
            this.name_text.Size = new System.Drawing.Size(259, 31);
            this.name_text.TabIndex = 35;
            this.name_text.Tag = "";
            // 
            // reset_button
            // 
            this.reset_button.Location = new System.Drawing.Point(344, 80);
            this.reset_button.Name = "reset_button";
            this.reset_button.Size = new System.Drawing.Size(108, 45);
            this.reset_button.TabIndex = 32;
            this.reset_button.Text = "Reset";
            this.reset_button.UseVisualStyleBackColor = true;
            // 
            // add_button
            // 
            this.add_button.Location = new System.Drawing.Point(143, 79);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(75, 45);
            this.add_button.TabIndex = 31;
            this.add_button.Text = "Add";
            this.add_button.UseVisualStyleBackColor = true;
            this.add_button.Click += new System.EventHandler(this.add_button_Click);
            // 
            // delete_button
            // 
            this.delete_button.Location = new System.Drawing.Point(225, 80);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(110, 44);
            this.delete_button.TabIndex = 40;
            this.delete_button.Text = "Delete";
            this.delete_button.UseVisualStyleBackColor = true;
            this.delete_button.Click += new System.EventHandler(this.delete_button_Click);
            // 
            // exit_button
            // 
            this.exit_button.Location = new System.Drawing.Point(458, 79);
            this.exit_button.Name = "exit_button";
            this.exit_button.Size = new System.Drawing.Size(106, 45);
            this.exit_button.TabIndex = 41;
            this.exit_button.Text = "Thoát";
            this.exit_button.UseVisualStyleBackColor = true;
            // 
            // directoryTableAdapter
            // 
            this.directoryTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iddirDataGridViewTextBoxColumn,
            this.namedirDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.directoryBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(26, 131);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 82;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(847, 278);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 25);
            this.label1.TabIndex = 42;
            this.label1.Text = "Tên danh mục";
            // 
            // iddirDataGridViewTextBoxColumn
            // 
            this.iddirDataGridViewTextBoxColumn.DataPropertyName = "iddir";
            this.iddirDataGridViewTextBoxColumn.HeaderText = "iddir";
            this.iddirDataGridViewTextBoxColumn.MinimumWidth = 10;
            this.iddirDataGridViewTextBoxColumn.Name = "iddirDataGridViewTextBoxColumn";
            this.iddirDataGridViewTextBoxColumn.Width = 80;
            // 
            // namedirDataGridViewTextBoxColumn
            // 
            this.namedirDataGridViewTextBoxColumn.DataPropertyName = "namedir";
            this.namedirDataGridViewTextBoxColumn.HeaderText = "namedir";
            this.namedirDataGridViewTextBoxColumn.MinimumWidth = 10;
            this.namedirDataGridViewTextBoxColumn.Name = "namedirDataGridViewTextBoxColumn";
            this.namedirDataGridViewTextBoxColumn.Width = 300;
            // 
            // QLDanhMuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 421);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.exit_button);
            this.Controls.Add(this.delete_button);
            this.Controls.Add(this.search_button);
            this.Controls.Add(this.name_text);
            this.Controls.Add(this.reset_button);
            this.Controls.Add(this.add_button);
            this.Name = "QLDanhMuc";
            this.Text = "Danh Mục";
            this.Load += new System.EventHandler(this.QuanLyDanhMuc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.directoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLtestDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button search_button;
        private System.Windows.Forms.TextBox name_text;
        private System.Windows.Forms.Button reset_button;
        private System.Windows.Forms.Button add_button;
        private System.Windows.Forms.Button delete_button;
        private System.Windows.Forms.Button exit_button;
        private QLtestDataSet1 qLtestDataSet1;
        private System.Windows.Forms.BindingSource directoryBindingSource;
        private QLtestDataSet1TableAdapters.directoryTableAdapter directoryTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iddirDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namedirDataGridViewTextBoxColumn;
    }
}