﻿namespace lab03_VD2
{
    partial class QuanLySanPham
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nMaMH = new System.Windows.Forms.TextBox();
            this.nTenMatHang = new System.Windows.Forms.TextBox();
            this.nDonViTinh = new System.Windows.Forms.TextBox();
            this.nGia = new System.Windows.Forms.TextBox();
            this.nCtySX = new System.Windows.Forms.TextBox();
            this.Add = new System.Windows.Forms.Button();
            this.Reset = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.nDanhMuc = new System.Windows.Forms.ComboBox();
            this.nTimKiem = new System.Windows.Forms.TextBox();
            this.TimKiem = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.listSPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listSPBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(286, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 46);
            this.label1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã MH";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(129, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên mặt hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(351, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Đơn vị tính";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(516, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 25);
            this.label5.TabIndex = 4;
            this.label5.Text = "Giá";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(677, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cty SX";
            // 
            // nMaMH
            // 
            this.nMaMH.Location = new System.Drawing.Point(47, 65);
            this.nMaMH.Name = "nMaMH";
            this.nMaMH.Size = new System.Drawing.Size(76, 31);
            this.nMaMH.TabIndex = 6;
            this.nMaMH.TextChanged += new System.EventHandler(this.nMaMH_TextChanged);
            // 
            // nTenMatHang
            // 
            this.nTenMatHang.Location = new System.Drawing.Point(129, 65);
            this.nTenMatHang.Name = "nTenMatHang";
            this.nTenMatHang.Size = new System.Drawing.Size(221, 31);
            this.nTenMatHang.TabIndex = 7;
            // 
            // nDonViTinh
            // 
            this.nDonViTinh.Location = new System.Drawing.Point(356, 65);
            this.nDonViTinh.Name = "nDonViTinh";
            this.nDonViTinh.Size = new System.Drawing.Size(159, 31);
            this.nDonViTinh.TabIndex = 8;
            // 
            // nGia
            // 
            this.nGia.Location = new System.Drawing.Point(521, 65);
            this.nGia.Name = "nGia";
            this.nGia.Size = new System.Drawing.Size(155, 31);
            this.nGia.TabIndex = 9;
            // 
            // nCtySX
            // 
            this.nCtySX.Location = new System.Drawing.Point(682, 65);
            this.nCtySX.Name = "nCtySX";
            this.nCtySX.Size = new System.Drawing.Size(192, 31);
            this.nCtySX.TabIndex = 10;
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(726, 137);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 45);
            this.Add.TabIndex = 11;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.button1_Click);
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(888, 137);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(85, 45);
            this.Reset.TabIndex = 12;
            this.Reset.Text = "Reset";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.button2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(875, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 25);
            this.label7.TabIndex = 13;
            this.label7.Text = "Danh Mục";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // nDanhMuc
            // 
            this.nDanhMuc.FormattingEnabled = true;
            this.nDanhMuc.Items.AddRange(new object[] {
            "test"});
            this.nDanhMuc.Location = new System.Drawing.Point(880, 65);
            this.nDanhMuc.Name = "nDanhMuc";
            this.nDanhMuc.Size = new System.Drawing.Size(175, 33);
            this.nDanhMuc.TabIndex = 14;
            // 
            // nTimKiem
            // 
            this.nTimKiem.Location = new System.Drawing.Point(46, 144);
            this.nTimKiem.Name = "nTimKiem";
            this.nTimKiem.Size = new System.Drawing.Size(469, 31);
            this.nTimKiem.TabIndex = 15;
            // 
            // TimKiem
            // 
            this.TimKiem.Location = new System.Drawing.Point(521, 137);
            this.TimKiem.Name = "TimKiem";
            this.TimKiem.Size = new System.Drawing.Size(110, 45);
            this.TimKiem.TabIndex = 16;
            this.TimKiem.Text = "Tìm kiếm";
            this.TimKiem.UseVisualStyleBackColor = true;
            this.TimKiem.Click += new System.EventHandler(this.TimKiem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(807, 137);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 45);
            this.button1.TabIndex = 20;
            this.button1.Text = "Edit";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(29, 233);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 82;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(1026, 268);
            this.dataGridView1.TabIndex = 21;
            // 
            // listSPBindingSource
            // 
            this.listSPBindingSource.DataSource = typeof(lab03_VD2.ListSP);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(173, 25);
            this.label8.TabIndex = 22;
            this.label8.Text = "Tên MH tìm kiếm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(326, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 25);
            this.label9.TabIndex = 23;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(637, 137);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 45);
            this.button2.TabIndex = 24;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(980, 137);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 45);
            this.button3.TabIndex = 25;
            this.button3.Text = "Thoát";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // QuanLySanPham
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1075, 556);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TimKiem);
            this.Controls.Add(this.nTimKiem);
            this.Controls.Add(this.nDanhMuc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.nCtySX);
            this.Controls.Add(this.nGia);
            this.Controls.Add(this.nDonViTinh);
            this.Controls.Add(this.nTenMatHang);
            this.Controls.Add(this.nMaMH);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "QuanLySanPham";
            this.Text = "Quản lý bán hàng";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listSPBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox nMaMH;
        private System.Windows.Forms.TextBox nTenMatHang;
        private System.Windows.Forms.TextBox nDonViTinh;
        private System.Windows.Forms.TextBox nGia;
        private System.Windows.Forms.TextBox nCtySX;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox nDanhMuc;
        private System.Windows.Forms.TextBox nTimKiem;
        private System.Windows.Forms.Button TimKiem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource listSPBindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

