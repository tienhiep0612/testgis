﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab03_VD2
{
    internal class SanPham
    {
        public SanPham()
        {
        }

        public int MaMH { get; set; }
        public String TenMH { get; set; }
        public int DonViTinh { get; set; }
        public int Gia { get; set; }
        public String CtySX { get; set; }
        public String DanhMuc { get; set; }

        public SanPham(int maMH, string tenMH, int donViTinh, int gia, string ctySX/*, String danhMuc*/)
        {
            MaMH = maMH;
            TenMH = tenMH;
            DonViTinh = donViTinh;
            Gia = gia;
            CtySX = ctySX;
            /*DanhMuc = danhMuc;*/
        }
        public SanPham(TextBox nMaMH, TextBox nTenMatHang, TextBox nDonViTinh, TextBox nGia, TextBox nCtySX)
            {
                // Kiểm tra và chuyển đổi giá trị từ TextBox thành kiểu tương ứng
                if (int.TryParse(nMaMH.Text, out int maMH))
                {
                    this.MaMH = maMH;
                }
                else
                {
                    MessageBox.Show("Giá trị không hợp lệ cho Mã mặt hàng.");
                }

                this.TenMH = nTenMatHang.Text;

                // Kiểm tra và chuyển đổi giá trị từ TextBox thành kiểu tương ứng
                if (int.TryParse(nDonViTinh.Text, out int donViTinh))
                {
                    this.DonViTinh = donViTinh;
                }
                else
                {
                    MessageBox.Show("Giá trị không hợp lệ cho Đơn vị tính.");
                }

                // Kiểm tra và chuyển đổi giá trị từ TextBox thành kiểu tương ứng
                if (int.TryParse(nGia.Text, out int gia))
                {
                    this.Gia = gia;
                }
                else
                {
                    MessageBox.Show("Giá trị không hợp lệ cho Giá.");
                }

                this.CtySX = nCtySX.Text;
        }
    }
}